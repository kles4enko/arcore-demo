package org.bitbucket.kles4enko.sceneform

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.google.ar.core.HitResult
import com.google.ar.sceneform.AnchorNode
import com.google.ar.sceneform.rendering.ModelRenderable
import com.google.ar.sceneform.ux.ArFragment
import com.google.ar.sceneform.ux.TransformableNode
import org.bitbucket.kles4enko.arcore.R


class SceneformActivity: AppCompatActivity() {

    private var andyRenderable: ModelRenderable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sceneform)

        ModelRenderable.builder()
            .setSource(this, R.raw.andy)
            .build()
            .thenAccept {  andyRenderable = it }
        setupTapListener()
    }

    private fun setupTapListener() {
        val arFragment = supportFragmentManager.findFragmentById(R.id.arContent) as ArFragment
        arFragment.setOnTapArPlaneListener { hitResult, plane, motionEvent ->
            andyRenderable?.let {
                val anchorNode = createAnchor(hitResult, arFragment)
                createAndyAndAddToAnchor(arFragment, anchorNode)
            }
        }
    }

    private fun createAnchor(hitResult: HitResult?, arFragment: ArFragment): AnchorNode {
        val anchor = hitResult?.createAnchor()
        val anchorNode = AnchorNode(anchor)
        anchorNode.setParent(arFragment.arSceneView.scene)
        return anchorNode
    }

    private fun createAndyAndAddToAnchor(arFragment: ArFragment, anchorNode: AnchorNode): Boolean {
        val andy = TransformableNode(arFragment.transformationSystem)
        andy.setParent(anchorNode)
        andy.renderable = andyRenderable
        return andy.select()
    }

}