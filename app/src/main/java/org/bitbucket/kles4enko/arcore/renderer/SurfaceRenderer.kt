package org.bitbucket.kles4enko.arcore.renderer

import android.opengl.GLES20
import android.opengl.GLSurfaceView
import com.google.ar.core.*
import org.bitbucket.kles4enko.arcore.fragment.ARFragment
import org.bitbucket.kles4enko.arcore.util.DisplayRotationListener
import org.bitbucket.kles4enko.arcore.util.TapHelper
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10
import com.google.ar.core.TrackingState




class SurfaceRenderer(
    private val sessionHolder: ARFragment.SessionHolder,
    private val cameraPreviewRenderer: CameraPreviewRenderer,
    private val displayRotationListener: DisplayRotationListener,
    private val pointCloudRenderer: PointCloudRenderer,
    private val planesRenderer: PlanesRenderer,
    private val objectRenderer: ObjectRenderer,
    private val  tapHelper: TapHelper,
    private val showDebugPoints: Boolean,
    private val showPlanes: Boolean,
    private val showCameraPreview: Boolean,
    private val showObjects: Boolean
) : GLSurfaceView.Renderer {

    private val anchors = ArrayList<ColoredAnchor>()
    private val defaultColors = floatArrayOf(0f, 0f, 0f, 0f)
    private val anchorMatrix = FloatArray(16)

    override fun onSurfaceCreated(gl: GL10?, config: EGLConfig?) {
        GLES20.glClearColor(0.1F, 0.1F, 0.1F, 1.0F)
        cameraPreviewRenderer.initOnGLThread()
        if (showDebugPoints) {
            pointCloudRenderer.initOnGLThread()
        }
        if (showPlanes) {
            planesRenderer.initOnGLThread()
        }
        if (showObjects) {
            objectRenderer.initOnGlThread()
            objectRenderer.setMaterialProperties(0.0f, 2.0f, 0.5f, 6.0f)
        }
    }

    override fun onSurfaceChanged(gl: GL10?, width: Int, height: Int) {
        displayRotationListener.onSurfaceChanged(width, height)
        GLES20.glViewport(0, 0, width, height)
    }

    override fun onDrawFrame(gl: GL10?) {
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT or GLES20.GL_DEPTH_BUFFER_BIT)
        sessionHolder.session?.let { session ->
            displayRotationListener.updateSessionIfNeeeded(session)
            session.setCameraTextureName(cameraPreviewRenderer.textureId())
            val frame = session.update()
            val camera = frame.camera
            if (showObjects) {
                handleTap(frame, camera)
            }
            if (showCameraPreview) {
                cameraPreviewRenderer.render(frame)
            }
            if (camera.trackingState == TrackingState.PAUSED) return // nothing to render
            val projectionMatrix = extractProjectionMatrix(camera)
            val viewMatrix = extractViewMatrix(camera)
            if (showDebugPoints) {
                frame.acquirePointCloud()?.let {
                    pointCloudRenderer.update(it)
                    pointCloudRenderer.render(viewMatrix, projectionMatrix)
                    it.release()
                }
            }
            if (showPlanes) {
                planesRenderer.render(
                    session.getAllTrackables(Plane::class.java),
                    camera.displayOrientedPose,
                    projectionMatrix
                )
            }
            if (showObjects) {
                renderAnchors(frame, viewMatrix, projectionMatrix)
            }
        }
    }

    private fun extractProjectionMatrix(camera: Camera): FloatArray {
        val projectionMatrix = FloatArray(16)
        val near = 0.1F
        val far = 100F
        camera.getProjectionMatrix(projectionMatrix, 0, near, far)
        return projectionMatrix
    }

    private fun extractViewMatrix(camera: Camera): FloatArray {
        val viewMatrix = FloatArray(16)
        camera.getViewMatrix(viewMatrix, 0)
        return viewMatrix
    }

    private fun handleTap(frame: Frame, camera: Camera) {
        val tap = tapHelper.poll()
        if (tap != null && camera.trackingState == TrackingState.TRACKING) {
            for (hit in frame.hitTest(tap)) {
                val trackable = hit.getTrackable()
                if ((trackable is Plane
                            && (trackable as Plane).isPoseInPolygon(hit.getHitPose())
                            && PlanesRenderer.calculateDistanceToPlane(
                        hit.getHitPose(),
                        camera.pose
                    ) > 0) || trackable is Point && (trackable as Point).getOrientationMode() === Point.OrientationMode.ESTIMATED_SURFACE_NORMAL
                ) {
                    if (anchors.size >= 20) {
                        anchors[0].anchor.detach()
                        anchors.removeAt(0)
                    }
                    val objColor: FloatArray = when (trackable) {
                        is Point -> floatArrayOf(66.0f, 133.0f, 244.0f, 255.0f)
                        is Plane -> floatArrayOf(139.0f, 195.0f, 74.0f, 255.0f)
                        else -> defaultColors
                    }
                    anchors.add(ColoredAnchor(hit.createAnchor(), objColor))
                    break
                }
            }
        }
    }

    private fun renderAnchors(
        frame: Frame,
        viewMatrix: FloatArray,
        projectionMatrix: FloatArray
    ) {
        val scaleFactor = 1.0f
        val colorCorrectionRgba = FloatArray(4)
        frame.lightEstimate.getColorCorrection(colorCorrectionRgba, 0)
        for ((anchor, color) in anchors) {
            if (anchor.trackingState !== TrackingState.TRACKING) {
                continue
            }
            anchor.pose.toMatrix(anchorMatrix, 0)
            objectRenderer.updateModelMatrix(anchorMatrix, scaleFactor)
            objectRenderer.draw(viewMatrix, projectionMatrix, colorCorrectionRgba, color)

        }
    }

    private data class ColoredAnchor(val anchor: Anchor, val color: FloatArray)

}