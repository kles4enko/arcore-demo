package org.bitbucket.kles4enko.arcore.util

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat

/**
 * Helper to ask camera permission.
 */
object CameraPermission {

    private const val CAMERA_PERMISSION_CODE = 0
    private const val CAMERA_PERMISSION = Manifest.permission.CAMERA

    /**
     * Check to see we have the necessary permissions for this app.
     */
    fun hasCameraPermission(context: Context) =
        ContextCompat.checkSelfPermission(context, CAMERA_PERMISSION) == PackageManager.PERMISSION_GRANTED


    /**
     * Check to see we have the necessary permissions for this app, and ask for them if we don't.
     */
    fun requestCameraPermission(activity: Activity) =
        ActivityCompat.requestPermissions(activity, arrayOf(CAMERA_PERMISSION), CAMERA_PERMISSION_CODE)

    /**
     * Check to see we have the necessary permissions for this app, and ask for them if we don't.
     */
    fun requestCameraPermission(fragment: Fragment) =
        fragment.requestPermissions(arrayOf(CAMERA_PERMISSION), CAMERA_PERMISSION_CODE)

    /**
     * Check to see if we need to show the rationale for this permission.
     */
    fun shouldShowRequestPermissionRationale(activity: Activity) =
        ActivityCompat.shouldShowRequestPermissionRationale(activity, CAMERA_PERMISSION)

    /**
     * Check to see if we need to show the rationale for this permission.
     */
    fun shouldShowRequestPermissionRationale(fragment: Fragment) =
        fragment.shouldShowRequestPermissionRationale(CAMERA_PERMISSION)

    /**
     * Launch Application Setting to grant permission.
     */
    fun launchPermissionSettings(activity: Activity) {
        val intent = Intent()
        intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
        intent.data = Uri.fromParts("package", activity.packageName, null)
        activity.startActivity(intent)
    }

}
