package org.bitbucket.kles4enko.arcore.fragment

import android.databinding.DataBindingUtil
import android.opengl.GLSurfaceView
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.ar.core.Session
import org.bitbucket.kles4enko.arcore.R
import org.bitbucket.kles4enko.arcore.databinding.FragmentArBinding
import org.bitbucket.kles4enko.arcore.renderer.*
import org.bitbucket.kles4enko.arcore.util.DisplayRotationListener
import org.bitbucket.kles4enko.arcore.util.TapHelper

class ARFragment : Fragment() {

    companion object {
        const val SHOW_CAMERA_PREVIEW_FLAG = true
        const val SHOW_DEBUG_POINTS_FLAG = true
        const val SHOW_PANES_FLAG = true
        const val SHOW_OBJECTS_FLAG = true
        fun newInstance() = ARFragment().apply {
            arguments = Bundle()
        }
    }

    private val arSessionHolder = SessionHolder(null)
    private lateinit var viewBinding: FragmentArBinding
    private lateinit var cameraPreviewRenderer: CameraPreviewRenderer
    private lateinit var displayRotationListener: DisplayRotationListener
    private lateinit var pointCloudRenderer: PointCloudRenderer
    private lateinit var planesRenderer: PlanesRenderer
    private lateinit var objectRenderer: ObjectRenderer
    private lateinit var tapHelper: TapHelper

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_ar, container, false)
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        cameraPreviewRenderer = CameraPreviewRenderer(requireContext())
        displayRotationListener = DisplayRotationListener(requireContext())
        pointCloudRenderer = PointCloudRenderer(requireContext())
        planesRenderer = PlanesRenderer(requireContext(), "textures/trigridSmall.png")
        objectRenderer = ObjectRenderer(requireContext(), "models/andy.obj", "models/andy.png")
        tapHelper = TapHelper(requireContext())
        initGLSurface(viewBinding.surfaceView, arSessionHolder, cameraPreviewRenderer,
            displayRotationListener, pointCloudRenderer, planesRenderer, objectRenderer, tapHelper)
    }

    override fun onPause() {
        super.onPause()
        viewBinding.surfaceView.onPause()
        arSessionHolder.session?.pause()
        displayRotationListener.onPause()
    }

    override fun onResume() {
        super.onResume()
        if (null == arSessionHolder.session) {
            arSessionHolder.session = createSession()
        }
        arSessionHolder.session?.resume()
        viewBinding.surfaceView.onResume()
        displayRotationListener.onResume()
    }

    private fun initGLSurface(
        surfaceView: GLSurfaceView,
        session: SessionHolder,
        cameraPreviewRenderer: CameraPreviewRenderer,
        displayRotationListener: DisplayRotationListener,
        pointCloudRenderer: PointCloudRenderer,
        planesRenderer: PlanesRenderer,
        objectRenderer: ObjectRenderer,
        tapHelper: TapHelper
    ) {
        val surfaceRenderer = SurfaceRenderer(session, cameraPreviewRenderer, displayRotationListener,
            pointCloudRenderer, planesRenderer, objectRenderer, tapHelper, SHOW_DEBUG_POINTS_FLAG, SHOW_PANES_FLAG,
            SHOW_CAMERA_PREVIEW_FLAG, SHOW_OBJECTS_FLAG)
        surfaceView.preserveEGLContextOnPause = true
        surfaceView.setEGLContextClientVersion(2)
        surfaceView.setEGLConfigChooser(8, 8, 8, 8, 16, 0)
        surfaceView.setRenderer(surfaceRenderer)
        surfaceView.setOnTouchListener(tapHelper)
        surfaceView.renderMode = GLSurfaceView.RENDERMODE_CONTINUOUSLY
    }

    private fun createSession(): Session {
        val session = Session(requireContext())
        /*val config = Config(session)
        config.focusMode = Config.FocusMode.FIXED
        config.lightEstimationMode = Config.LightEstimationMode.DISABLED
        config.updateMode = Config.UpdateMode.LATEST_CAMERA_IMAGE
        session.configure(config)*/
        return session
    }

    data class SessionHolder(var session: Session?)
}