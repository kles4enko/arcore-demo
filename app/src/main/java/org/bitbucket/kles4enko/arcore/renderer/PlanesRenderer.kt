package org.bitbucket.kles4enko.arcore.renderer

import android.content.Context
import android.graphics.BitmapFactory
import android.opengl.GLES20
import android.opengl.GLUtils
import android.opengl.Matrix
import com.google.ar.core.Plane
import com.google.ar.core.Pose
import com.google.ar.core.TrackingState
import org.bitbucket.kles4enko.arcore.util.ShaderLoader
import org.bitbucket.kles4enko.arcore.util.ShaderLoader.checkGLError
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer

class PlanesRenderer(
    private val context: Context,
    private val gridDistanceTextureName: String
) {

    companion object {
        private val TAG = PlanesRenderer::class.java.simpleName
        private const val VERTEX_SHADER_NAME = "shaders/plane.vert"
        private const val FRAGMENT_SHADER_NAME = "shaders/plane.frag"
        private const val COORDS_PER_VERTEX = 3 // x, z, alpha
        private const val VERTS_PER_BOUNDARY_VERT = 2
        private const val INDICES_PER_BOUNDARY_VERT = 3
        private const val INITIAL_BUFFER_BOUNDARY_VERTS = 64
        private const val INITIAL_VERTEX_BUFFER_SIZE_BYTES = (
                java.lang.Float.BYTES
                        * COORDS_PER_VERTEX
                        * VERTS_PER_BOUNDARY_VERT
                        * INITIAL_BUFFER_BOUNDARY_VERTS)
        private const val INITIAL_INDEX_BUFFER_SIZE_BYTES = (
                java.lang.Short.BYTES
                        * INDICES_PER_BOUNDARY_VERT
                        * INDICES_PER_BOUNDARY_VERT
                        * INITIAL_BUFFER_BOUNDARY_VERTS)
        private const val FADE_RADIUS_M = 0.25f
        private const val DOTS_PER_METER = 10.0f
        private val EQUILATERAL_TRIANGLE_SCALE = (1 / Math.sqrt(3.0)).toFloat()

        /** Calculate the normal distance to plane from cameraPose, the given planePose should have y axis
         * parallel to plane's normal, for example plane's center pose or hit test pose.
         */
        fun calculateDistanceToPlane(planePose: Pose, cameraPose: Pose): Float {
            val normal = FloatArray(3)
            val cameraX = cameraPose.tx()
            val cameraY = cameraPose.ty()
            val cameraZ = cameraPose.tz()
            // Get transformed Y axis of plane's coordinate system.
            planePose.getTransformedAxis(1, 1.0f, normal, 0)
            // Compute dot product of plane's normal with vector from camera to plane center.
            return ((cameraX - planePose.tx()) * normal[0]
                    + (cameraY - planePose.ty()) * normal[1]
                    + (cameraZ - planePose.tz()) * normal[2])
        }

        /** Using the "signed distance field" approach to render sharp lines and circles.
         * {dotThreshold, lineThreshold, lineFadeSpeed, occlusionScale}
         * dotThreshold/lineThreshold: red/green intensity above which dots/lines are present
         * lineFadeShrink:  lines will fade in between alpha = 1-(1/lineFadeShrink) and 1.0
         * occlusionShrink: occluded planes will fade out between alpha = 0 and 1/occlusionShrink
         */
        private val GRID_CONTROL = floatArrayOf(0.2f, 0.4f, 2.0f, 1.5f)

        private fun colorRgbaToFloat(planeColor: FloatArray, colorRgba: Int) {
            planeColor[0] = (colorRgba shr 24 and 0xff).toFloat() / 255.0f
            planeColor[1] = (colorRgba shr 16 and 0xff).toFloat() / 255.0f
            planeColor[2] = (colorRgba shr 8 and 0xff).toFloat() / 255.0f
            planeColor[3] = (colorRgba shr 0 and 0xff).toFloat() / 255.0f
        }

        private val PLANE_COLORS_RGBA = intArrayOf(
            0xFFFFFFFF.toInt(),
            0xF44336FF.toInt(),
            0xE91E63FF.toInt(),
            0x9C27B0FF.toInt(),
            0x673AB7FF.toInt(),
            0x3F51B5FF.toInt(),
            0x2196F3FF.toInt(),
            0x03A9F4FF.toInt(),
            0x00BCD4FF.toInt(),
            0x009688FF.toInt(),
            0x4CAF50FF.toInt(),
            0x8BC34AFF.toInt(),
            0xCDDC39FF.toInt(),
            0xFFEB3BFF.toInt(),
            0xFFC107FF.toInt(),
            0xFF9800FF.toInt()
        )
    }

    // Temporary lists/matrices allocated here to reduce number of allocations for each frame.
    private val modelMatrix = FloatArray(16)
    private val modelViewMatrix = FloatArray(16)
    private val modelViewProjectionMatrix = FloatArray(16)
    private val planeColor = FloatArray(4)
    private val planeAngleUVMatrix = FloatArray(4) // 2x2 rotation matrix applied to UV coords.

    private val planes = HashMap<Plane, Int>()

    private var shaderProgram = 0
    private var textureId = -1

    private var planeXZPositionAlphaAttribute: Int = 0
    private var planeModelUniform: Int = 0
    private var planeNormalUniform: Int = 0
    private var planeModelViewProjectionUniform: Int = 0
    private var textureUniform: Int = 0
    private var lineColorUniform: Int = 0
    private var dotColorUniform: Int = 0
    private var gridControlUniform: Int = 0
    private var planeUVMatrixUniform: Int = 0

    private var vertexBuffer = ByteBuffer.allocateDirect(INITIAL_VERTEX_BUFFER_SIZE_BYTES)
        .order(ByteOrder.nativeOrder())
        .asFloatBuffer()
    private var indexBuffer = ByteBuffer.allocateDirect(INITIAL_INDEX_BUFFER_SIZE_BYTES)
        .order(ByteOrder.nativeOrder())
        .asShortBuffer()

    fun initOnGLThread() {
        initShadersAndProgram(context)
        initGridTexture()
        bindShaderParams()
    }

    fun render(allPlanes: Collection<Plane>, cameraPose: Pose, cameraPerspective: FloatArray) {
        val sortedPlanes = sortPlanes(allPlanes, cameraPose)
        // Planes are drawn with additive blending, masked by the alpha channel for occlusion.
        clearingAlphaChannel()
        disableDepthWrite()
        enablingBlending()
        setupShader()
        attachTexture()
        setupGridControlUniformAndVertexArrays()
        val cameraView = extractCameraView(cameraPose)
        sortedPlanes.forEach {
            val plane = it.plane
            val planeMatrix = extractPlaneMatrix(plane)
            val normal = extractNormal(plane)
            updatePlaneParameters(planeMatrix, plane.extentX, plane.extentZ, plane.polygon)
            val planeIndex = getPlaneIndex(plane)
            setPlaneColor(planeIndex)
            computePlaneRotation(planeIndex)
            draw(cameraView, cameraPerspective, normal)
        }
        postRenderCleanup()
    }

    private fun draw(cameraView: FloatArray, cameraPerspective: FloatArray, normal: FloatArray) {
        // Build the ModelView and ModelViewProjection matrices
        // for calculating cube position and light.
        Matrix.multiplyMM(modelViewMatrix, 0, cameraView, 0, modelMatrix, 0)
        Matrix.multiplyMM(modelViewProjectionMatrix, 0, cameraPerspective, 0, modelViewMatrix, 0)
        // Set the position of the plane
        vertexBuffer.rewind()
        GLES20.glVertexAttribPointer(
            planeXZPositionAlphaAttribute,
            COORDS_PER_VERTEX,
            GLES20.GL_FLOAT,
            false,
            java.lang.Float.BYTES * COORDS_PER_VERTEX,
            vertexBuffer
        )
        // Set the Model and ModelViewProjection matrices in the shader.
        GLES20.glUniformMatrix4fv(planeModelUniform, 1, false, modelMatrix, 0)
        GLES20.glUniform3f(planeNormalUniform, normal[0], normal[1], normal[2])
        GLES20.glUniformMatrix4fv(planeModelViewProjectionUniform, 1, false, modelViewProjectionMatrix, 0)
        indexBuffer.rewind()
        GLES20.glDrawElements(GLES20.GL_TRIANGLE_STRIP, indexBuffer.limit(), GLES20.GL_UNSIGNED_SHORT, indexBuffer)
        checkGLError(TAG, "Drawing plane")
    }

    private fun updatePlaneParameters(planeMatrix: FloatArray, extentX: Float, extentZ: Float, boundary: FloatBuffer?) {
        System.arraycopy(planeMatrix, 0, modelMatrix, 0, 16)
        if (boundary == null) {
            vertexBuffer.limit(0)
            indexBuffer.limit(0)
            return
        }
        // Generate a new set of vertices and a corresponding triangle strip index set so that
        // the plane boundary polygon has a fading edge. This is done by making a copy of the
        // boundary polygon vertices and scaling it down around center to push it inwards. Then
        // the index buffer is setup accordingly.
        boundary.rewind()
        val boundaryVertices = boundary.limit() / 2
        val numVertices = boundaryVertices * VERTS_PER_BOUNDARY_VERT
        // drawn as GL_TRIANGLE_STRIP with 3n-2 triangles (n-2 for fill, 2n for perimeter).
        val numIndices = boundaryVertices * INDICES_PER_BOUNDARY_VERT
        if (vertexBuffer.capacity() < numVertices * COORDS_PER_VERTEX) {
            var size = vertexBuffer.capacity()
            while (size < numVertices * COORDS_PER_VERTEX) {
                size *= 2
            }
            vertexBuffer = ByteBuffer.allocateDirect(java.lang.Float.BYTES * size)
                .order(ByteOrder.nativeOrder())
                .asFloatBuffer()
        }
        vertexBuffer.rewind()
        vertexBuffer.limit(numVertices * COORDS_PER_VERTEX)

        if (indexBuffer.capacity() < numIndices) {
            var size = indexBuffer.capacity()
            while (size < numIndices) {
                size *= 2
            }
            indexBuffer = ByteBuffer.allocateDirect(java.lang.Short.BYTES * size)
                .order(ByteOrder.nativeOrder())
                .asShortBuffer()
        }
        indexBuffer.rewind()
        indexBuffer.limit(numIndices)

        // Note: when either dimension of the bounding box is smaller than 2*FADE_RADIUS_M we
        // generate a bunch of 0-area triangles.  These don't get rendered though so it works
        // out ok.
        val xScale = Math.max((extentX - 2 * FADE_RADIUS_M) / extentX, 0.0f)
        val zScale = Math.max((extentZ - 2 * FADE_RADIUS_M) / extentZ, 0.0f)

        while (boundary.hasRemaining()) {
            val x = boundary.get()
            val z = boundary.get()
            vertexBuffer.put(x)
            vertexBuffer.put(z)
            vertexBuffer.put(0.0f)
            vertexBuffer.put(x * xScale)
            vertexBuffer.put(z * zScale)
            vertexBuffer.put(1.0f)
        }

        // step 1, perimeter
        indexBuffer.put(((boundaryVertices - 1) * 2).toShort())
        for (i in 0 until boundaryVertices) {
            indexBuffer.put((i * 2).toShort())
            indexBuffer.put((i * 2 + 1).toShort())
        }
        indexBuffer.put(1.toShort())
        // This leaves us on the interior edge of the perimeter between the inset vertices
        // for boundary verts n-1 and 0.

        // step 2, interior:
        for (i in 1 until boundaryVertices / 2) {
            indexBuffer.put(((boundaryVertices - 1 - i) * 2 + 1).toShort())
            indexBuffer.put((i * 2 + 1).toShort())
        }
        if (boundaryVertices % 2 != 0) {
            indexBuffer.put((boundaryVertices / 2 * 2 + 1).toShort())
        }
    }

    private fun loadShaders(context: Context): Pair<Int, Int> {
        val vertexShader = ShaderLoader.loadGLShader(TAG, context, GLES20.GL_VERTEX_SHADER, VERTEX_SHADER_NAME)
        val fragmentShader = ShaderLoader.loadGLShader(TAG, context, GLES20.GL_FRAGMENT_SHADER, FRAGMENT_SHADER_NAME)
        return Pair(vertexShader, fragmentShader)
    }

    private fun initShadersAndProgram(context: Context) {
        val (vertex, fragment) = loadShaders(context)
        shaderProgram = GLES20.glCreateProgram()
        GLES20.glAttachShader(shaderProgram, vertex)
        GLES20.glAttachShader(shaderProgram, fragment)
        GLES20.glLinkProgram(shaderProgram)
        GLES20.glUseProgram(shaderProgram)
        checkGLError(TAG, "Shader program creating")
    }

    private fun initGridTexture() {
        val textureBitmap = BitmapFactory.decodeStream(context.assets.open(gridDistanceTextureName))
        val textures = IntArray(1)
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
        GLES20.glGenTextures(1, textures, 0)
        textureId = textures[0]
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR_MIPMAP_LINEAR)
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR)
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, textureBitmap, 0)
        GLES20.glGenerateMipmap(GLES20.GL_TEXTURE_2D)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0) // unbind texture
        checkGLError(TAG, "Texture loading")
    }

    private fun bindShaderParams() {
        planeXZPositionAlphaAttribute = GLES20.glGetAttribLocation(shaderProgram, "a_XZPositionAlpha")
        planeModelUniform = GLES20.glGetUniformLocation(shaderProgram, "u_Model")
        planeNormalUniform = GLES20.glGetUniformLocation(shaderProgram, "u_Normal")
        planeModelViewProjectionUniform = GLES20.glGetUniformLocation(shaderProgram, "u_ModelViewProjection")
        textureUniform = GLES20.glGetUniformLocation(shaderProgram, "u_Texture")
        lineColorUniform = GLES20.glGetUniformLocation(shaderProgram, "u_lineColor")
        dotColorUniform = GLES20.glGetUniformLocation(shaderProgram, "u_dotColor")
        gridControlUniform = GLES20.glGetUniformLocation(shaderProgram, "u_gridControl")
        planeUVMatrixUniform = GLES20.glGetUniformLocation(shaderProgram, "u_PlaneUvMatrix")
        checkGLError(TAG, "Program parameters")
    }

    private fun enablingBlending() {
        GLES20.glEnable(GLES20.GL_BLEND)
        GLES20.glBlendFuncSeparate(
            GLES20.GL_DST_ALPHA, GLES20.GL_ONE, // RGB (src, dest)
            GLES20.GL_ZERO, GLES20.GL_ONE_MINUS_SRC_ALPHA
        ) // ALPHA (src, dest)
    }

    private fun disableDepthWrite() {
        GLES20.glDepthMask(false)
    }

    private fun clearingAlphaChannel() {
        GLES20.glClearColor(1F, 1F, 1F, 1F)
        GLES20.glColorMask(false, false, false, true)
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT)
        GLES20.glColorMask(true, true, true, true)
    }

    private fun setupShader() {
        GLES20.glUseProgram(shaderProgram)
    }

    private fun attachTexture() {
        GLES20.glActiveTexture(GLES20.GL_TEXTURE0)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, textureId)
        GLES20.glUniform1i(textureUniform, 0)
    }

    private fun setupGridControlUniformAndVertexArrays() {
        // Shared fragment uniforms.
        GLES20.glUniform4fv(gridControlUniform, 1, GRID_CONTROL, 0)
        // Enable vertex arrays
        GLES20.glEnableVertexAttribArray(planeXZPositionAlphaAttribute)
        checkGLError(TAG, "Setting up to draw planes")
    }

    private fun extractCameraView(cameraPose: Pose): FloatArray {
        val cameraView = FloatArray(16)
        cameraPose.inverse().toMatrix(cameraView, 0)
        return cameraView
    }

    private fun extractNormal(plane: Plane): FloatArray {
        val normal = FloatArray(3)
        // Get transformed Y axis of plane's coordinate system.
        plane.centerPose.getTransformedAxis(1, 1.0F, normal, 0)
        return normal
    }

    private fun extractPlaneMatrix(plane: Plane): FloatArray {
        val planeMatrix = FloatArray(16)
        plane.centerPose.toMatrix(planeMatrix, 0)
        return planeMatrix
    }

    private fun sortPlanes(allPlanes: Collection<Plane>, cameraPose: Pose): MutableList<SortedPlane> {
        val sortedPlanes = mutableListOf<SortedPlane>()
        allPlanes
            .filter { it.trackingState != TrackingState.TRACKING || it.subsumedBy != null }
            .forEach {
                val distance = calculateDistanceToPlane(it.centerPose, cameraPose)
                if (distance >= 0) {
                    sortedPlanes.add(SortedPlane(distance, it))
                }
            }
        sortedPlanes.sort()
        return sortedPlanes
    }

    private fun getPlaneIndex(plane: Plane): Int {
        // Get plane index. Keep a map to assign same indices to same planes.
        var planeIndex = planes[plane]
        if (planeIndex == null) {
            planeIndex = planes.size
            planes[plane] = planeIndex
        }
        return planeIndex
    }

    private fun setPlaneColor(planeIndex: Int) {
        val colorIndex = planeIndex % PLANE_COLORS_RGBA.size
        colorRgbaToFloat(planeColor, PLANE_COLORS_RGBA[colorIndex])
        GLES20.glUniform4fv(lineColorUniform, 1, planeColor, 0)
        GLES20.glUniform4fv(dotColorUniform, 1, planeColor, 0)
    }

    private fun computePlaneRotation(planeIndex: Int) {
        // Each plane will have its own angle offset from others, to make them easier to
        // distinguish. Compute a 2x2 rotation matrix from the angle.
        val angleRadians = planeIndex * 0.144F
        val uScale = DOTS_PER_METER
        val vScale = DOTS_PER_METER * EQUILATERAL_TRIANGLE_SCALE
        planeAngleUVMatrix[0] = +Math.cos(angleRadians.toDouble()).toFloat() * uScale
        planeAngleUVMatrix[1] = -Math.sin(angleRadians.toDouble()).toFloat() * vScale
        planeAngleUVMatrix[2] = +Math.sin(angleRadians.toDouble()).toFloat() * uScale
        planeAngleUVMatrix[3] = +Math.cos(angleRadians.toDouble()).toFloat() * vScale
        GLES20.glUniformMatrix2fv(planeUVMatrixUniform, 1, false, planeAngleUVMatrix, 0)
    }


    private fun postRenderCleanup() {
        GLES20.glDisableVertexAttribArray(planeXZPositionAlphaAttribute)
        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, 0)
        GLES20.glDisable(GLES20.GL_BLEND)
        GLES20.glDepthMask(true)
        checkGLError(TAG, "Cleaning up after drawing planes")
    }

    data class SortedPlane(val distance: Float, val plane: Plane) : Comparable<SortedPlane> {
        override fun compareTo(other: SortedPlane): Int = java.lang.Float.compare(this.distance, other.distance)
    }
}