package org.bitbucket.kles4enko.arcore.renderer

import android.content.Context
import android.opengl.GLES11Ext
import android.opengl.GLES20
import com.google.ar.core.Frame
import org.bitbucket.kles4enko.arcore.util.ShaderLoader
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer


class CameraPreviewRenderer(private val context: Context) {

    companion object {
        private val TAG = CameraPreviewRenderer::class.java.simpleName
        private const val VERTEX_SHADER_NAME = "shaders/screenquad.vert"
        private const val FRAGMENT_SHADER_NAME = "shaders/screenquad.frag"
        private const val COORDS_PER_VERTEX = 3
        private const val TEXCOORDS_PER_VERTEX = 2
        private const val FLOAT_SIZE = 4

        private val QUAD_COORDS = floatArrayOf(
            -1.0f, -1.0f, 0.0f,
            -1.0f, +1.0f, 0.0f,
            +1.0f, -1.0f, 0.0f,
            +1.0f, +1.0f, 0.0f
        )
        private val QUAD_TEX_COORDS = floatArrayOf(
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            1.0f, 0.0f
        )
    }

    private lateinit var quadVertices: FloatBuffer
    private lateinit var quadTexCoord: FloatBuffer
    private lateinit var quadTexCoordTransformed: FloatBuffer

    private var shaderProgram = 0
    private var quadPositionParam = 0
    private var quadTexCoordParam = 0
    private var textureId = -1

    fun initOnGLThread() {
        initTexture()
        checkVertices()
        initQuadCoordsBuffer()
        initQuadTextureBuffer()
        initQuadTextureTransformedBuffer()
        initShadersAndProgram(context)
        bindShaderParams()
    }

    fun render(frame: Frame) {
        checkRotation(frame)
        disablingDepthTest()
        bindTexture()
        enableProgram()
        setVertexPositions()
        setTextureCoordinates()
        enableVertexArrays()
        render()
        disabledVertexArrays()
        enabledDepthTest()
        ShaderLoader.checkGLError(TAG, "onDraw")
    }

    fun textureId() = textureId

    private fun initTexture() {
        val textures = IntArray(1)
        GLES20.glGenTextures(1, textures, 0)
        textureId = textures[0]
        val textureTarget = GLES11Ext.GL_TEXTURE_EXTERNAL_OES
        GLES20.glBindTexture(textureTarget, textureId)
        GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE)
        GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE)
        GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST)
        GLES20.glTexParameteri(textureTarget, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST)
    }

    private fun checkVertices() {
        val verticesNumber = 4
        if (verticesNumber != QUAD_COORDS.size / COORDS_PER_VERTEX) {
            throw IllegalArgumentException("Unexpected number of vertices")
        }
    }

    private fun initQuadCoordsBuffer() {
        val quadCoordsBuffer = ByteBuffer.allocateDirect(QUAD_COORDS.size * FLOAT_SIZE)
        quadCoordsBuffer.order(ByteOrder.nativeOrder())
        quadVertices = quadCoordsBuffer.asFloatBuffer()
        quadVertices.put(QUAD_COORDS)
        quadVertices.position(0)
    }

    private fun initQuadTextureBuffer() {
        val quadTexCoordsBuffer = ByteBuffer.allocateDirect(QUAD_TEX_COORDS.size * FLOAT_SIZE)
        quadTexCoordsBuffer.order(ByteOrder.nativeOrder())
        quadTexCoord = quadTexCoordsBuffer.asFloatBuffer()
        quadTexCoord.put(QUAD_TEX_COORDS)
        quadTexCoord.position(0)
    }

    private fun initQuadTextureTransformedBuffer() {
        val quadTextCoordsTransformedBuffer = ByteBuffer.allocateDirect(QUAD_TEX_COORDS.size * FLOAT_SIZE)
        quadTextCoordsTransformedBuffer.order(ByteOrder.nativeOrder())
        quadTexCoordTransformed = quadTextCoordsTransformedBuffer.asFloatBuffer()
    }

    private fun loadShaders(context: Context): Pair<Int, Int> {
        val vertexShader = ShaderLoader.loadGLShader(TAG, context, GLES20.GL_VERTEX_SHADER, VERTEX_SHADER_NAME)
        val fragmentShader = ShaderLoader.loadGLShader(TAG, context, GLES20.GL_FRAGMENT_SHADER, FRAGMENT_SHADER_NAME)
        return Pair(vertexShader, fragmentShader)
    }

    private fun initShadersAndProgram(context: Context) {
        val (vertex, fragment) = loadShaders(context)
        shaderProgram = GLES20.glCreateProgram()
        GLES20.glAttachShader(shaderProgram, vertex)
        GLES20.glAttachShader(shaderProgram, fragment)
        GLES20.glLinkProgram(shaderProgram)
        GLES20.glUseProgram(shaderProgram)
        ShaderLoader.checkGLError(TAG, "Shader program creating")
    }

    private fun bindShaderParams() {
        quadPositionParam = GLES20.glGetAttribLocation(shaderProgram, "a_Position")
        quadTexCoordParam = GLES20.glGetAttribLocation(shaderProgram, "a_TexCoord")
        ShaderLoader.checkGLError(TAG, "Linking program parameters")
    }

    private fun enableProgram() {
        GLES20.glUseProgram(shaderProgram)
    }

    private fun enabledDepthTest() {
        GLES20.glDepthMask(true)
        GLES20.glEnable(GLES20.GL_DEPTH_TEST)
    }

    private fun disabledVertexArrays() {
        GLES20.glDisableVertexAttribArray(quadPositionParam)
        GLES20.glDisableVertexAttribArray(quadTexCoordParam)
    }

    private fun render() {
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4)
    }

    private fun enableVertexArrays() {
        GLES20.glEnableVertexAttribArray(quadPositionParam)
        GLES20.glEnableVertexAttribArray(quadTexCoordParam)
    }

    private fun setTextureCoordinates() {
        GLES20.glVertexAttribPointer(
            quadTexCoordParam,
            TEXCOORDS_PER_VERTEX,
            GLES20.GL_FLOAT,
            false,
            0,
            quadTexCoordTransformed
        )
    }

    private fun setVertexPositions() {
        GLES20.glVertexAttribPointer(
            quadPositionParam,
            COORDS_PER_VERTEX,
            GLES20.GL_FLOAT,
            false,
            0,
            quadVertices
        )
    }

    private fun bindTexture() {
        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, textureId)
    }

    private fun disablingDepthTest() {
        GLES20.glDisable(GLES20.GL_DEPTH_TEST)
        GLES20.glDepthMask(false)
    }

    /**
     *   If display rotation changed (also includes view size change), we need to re-query the uv
     *   coordinates for the screen rect, as they may have changed as well.
     */
    private fun checkRotation(frame: Frame) {
        if (frame.hasDisplayGeometryChanged()) {
            frame.transformDisplayUvCoords(quadTexCoord, quadTexCoordTransformed)
        }
    }

}
