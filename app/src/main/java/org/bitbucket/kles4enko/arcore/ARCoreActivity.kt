package org.bitbucket.kles4enko.arcore

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.WindowManager
import org.bitbucket.kles4enko.arcore.databinding.ActivityMainBinding
import org.bitbucket.kles4enko.arcore.fragment.ARFragment
import org.bitbucket.kles4enko.arcore.fragment.RequestPermissionFragment
import org.bitbucket.kles4enko.arcore.fragment.StartFragment

class ARCoreActivity : AppCompatActivity() {

    private lateinit var viewBinding: ActivityMainBinding
    private lateinit var viewModel: ARCoreViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(ARCoreViewModel::class.java)
        viewModel.toPermissionsScreen()
        setSupportActionBar(viewBinding.toolbar)
        enableAlwaysOn()
    }

    override fun onStart() {
        super.onStart()
        setupScreenListeners()
    }

    private fun setupScreenListeners() {
        viewModel.permissionScreenEvent().observe(this, Observer {
            if (it == null || !it) {
                return@Observer
            } else {
                val fragment = RequestPermissionFragment.newInstance()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .commit()
            }
        })
        viewModel.startScreenEvent().observe(this, Observer {
            if (it == null || !it) {
                return@Observer
            } else {
                val fragment = StartFragment.newInstance()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .commit()
            }
        })
        viewModel.arCoreScreenEvent().observe(this, Observer {
            if (it == null || !it) {
                return@Observer
            } else {
                val fragment = ARFragment.newInstance()
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragmentContainer, fragment)
                    .addToBackStack("arCoreScreen")
                    .commit()
            }
        })
    }

    private fun enableAlwaysOn() {
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    }

}
