package org.bitbucket.kles4enko.arcore

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import org.bitbucket.kles4enko.arcore.util.SingleLiveEvent

class ARCoreViewModel : ViewModel() {

    private val toPermissionsScreen = SingleLiveEvent<Boolean>()
    private val toStartScreen = SingleLiveEvent<Boolean>()
    private val toARCoreScreen = SingleLiveEvent<Boolean>()

    fun toPermissionsScreen() = toPermissionsScreen.postValue(true)
    fun toStartScreen() = toStartScreen.postValue(true)
    fun toARCoreScreen() = toARCoreScreen.postValue(true)

    fun permissionScreenEvent() = toPermissionsScreen as LiveData<Boolean>
    fun startScreenEvent() = toStartScreen as LiveData<Boolean>
    fun arCoreScreenEvent() = toARCoreScreen as LiveData<Boolean>

}