package org.bitbucket.kles4enko.arcore.util

import android.content.Context
import android.hardware.display.DisplayManager
import android.view.WindowManager
import com.google.ar.core.Session


class DisplayRotationListener(private val context: Context) : DisplayManager.DisplayListener {

    private val display = context.getSystemService(WindowManager::class.java).defaultDisplay
    private var viewportChanged = false
    private var viewportWidth = 0
    private var viewportHeight = 0

    fun onResume() {
        context.getSystemService(DisplayManager::class.java).registerDisplayListener(this, null)
    }

    fun onPause() {
        context.getSystemService(DisplayManager::class.java).unregisterDisplayListener(this)
    }

    fun onSurfaceChanged(width: Int, height: Int) {
        viewportWidth = width
        viewportHeight = height
        viewportChanged = true
    }

    fun updateSessionIfNeeeded(session: Session) {
        if (viewportChanged) {
            display?.let {
                session.setDisplayGeometry(it.rotation, viewportWidth, viewportHeight)
                viewportChanged = false
            }
        }
    }

    fun getRotation() = display?.rotation

    override fun onDisplayAdded(displayId: Int) { /* no-op */ }

    override fun onDisplayRemoved(displayId: Int) { /* no-op */ }

    override fun onDisplayChanged(displayId: Int) {
        viewportChanged = true
    }

}