package org.bitbucket.kles4enko.arcore.fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.bitbucket.kles4enko.arcore.ARCoreViewModel
import org.bitbucket.kles4enko.arcore.R
import org.bitbucket.kles4enko.arcore.databinding.FragmentStartBinding

class StartFragment : Fragment() {

    companion object {
        fun newInstance() = StartFragment().apply {
            arguments = Bundle()
        }
    }

    private lateinit var viewBinding: FragmentStartBinding
    private lateinit var viewModel: ARCoreViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_start, container, false)
        viewModel = ViewModelProviders.of(requireActivity()).get(ARCoreViewModel::class.java)
        viewBinding.startArButton.setOnClickListener { viewModel.toARCoreScreen() }
        return viewBinding.root
    }

}