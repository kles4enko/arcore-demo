package org.bitbucket.kles4enko.arcore.fragment

import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.toast
import com.google.ar.core.ArCoreApk
import com.google.ar.core.exceptions.UnsupportedConfigurationException
import org.bitbucket.kles4enko.arcore.ARCoreViewModel
import org.bitbucket.kles4enko.arcore.R
import org.bitbucket.kles4enko.arcore.databinding.FragmentRequestPermissionsBinding
import org.bitbucket.kles4enko.arcore.util.CameraPermission.hasCameraPermission
import org.bitbucket.kles4enko.arcore.util.CameraPermission.launchPermissionSettings
import org.bitbucket.kles4enko.arcore.util.CameraPermission.requestCameraPermission
import org.bitbucket.kles4enko.arcore.util.CameraPermission.shouldShowRequestPermissionRationale

class RequestPermissionFragment : Fragment() {

    companion object {
        private val TAG = RequestPermissionFragment::class.java.simpleName

        fun newInstance() = RequestPermissionFragment().apply {
            arguments = Bundle()
        }
    }

    private lateinit var viewBinding: FragmentRequestPermissionsBinding
    private lateinit var viewModel: ARCoreViewModel

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(requireActivity()).get(ARCoreViewModel::class.java)
        checkArCoreApk()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_request_permissions, container, false)
        return viewBinding.root
    }

    override fun onResume() {
        super.onResume()
        // ARCore requires camera permission to operate.
        if (!hasCameraPermission(requireContext())) {
            showProgress()
            requestCameraPermission(this)
        } else {
            hideProgressAndMoveFurther()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, results: IntArray) {
        if (!hasCameraPermission(requireActivity())) {
            requireContext().toast("Camera permission is needed to run this application", Toast.LENGTH_LONG)
            if (!shouldShowRequestPermissionRationale(this)) {
                // Permission denied with checking "Do not ask again".
                launchPermissionSettings(requireActivity())
            }
            requireActivity().finishAfterTransition()
        } else {
            hideProgressAndMoveFurther()
        }
    }

    private fun checkArCoreApk() {
        val availability = ArCoreApk.getInstance().checkAvailability(requireContext())
        Log.i(TAG, "ARCore supported: ${availability.isSupported}")
        if (availability.isUnsupported) {
            requireContext().toast("ARCore not supported by device", Toast.LENGTH_LONG)
            throw UnsupportedConfigurationException()
        }
    }

    private fun showProgress() {
        viewBinding.successText.visibility = View.GONE
        viewBinding.progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressAndMoveFurther() {
        viewBinding.successText.visibility = View.VISIBLE
        viewBinding.progressBar.visibility = View.GONE
        viewModel.toStartScreen()
    }

}