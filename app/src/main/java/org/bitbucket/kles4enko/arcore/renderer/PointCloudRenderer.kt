package org.bitbucket.kles4enko.arcore.renderer

import android.content.Context
import android.opengl.GLES20
import android.opengl.Matrix
import com.google.ar.core.PointCloud
import org.bitbucket.kles4enko.arcore.util.ShaderLoader
import org.bitbucket.kles4enko.arcore.util.ShaderLoader.checkGLError

class PointCloudRenderer(private val context: Context) {

    companion object {
        private val TAG = PointCloudRenderer::class.java.simpleName
        private const val VERTEX_SHADER_NAME = "shaders/pointCloud.vert"
        private const val FRAGMENT_SHADER_NAME = "shaders/pointCloud.frag"
        private const val FLOATS_PER_POINT = 4 // X,Y,Z,confidence.
        private const val BYTES_PER_POINT = java.lang.Float.BYTES * FLOATS_PER_POINT
        private const val INITIAL_BUFFER_POINTS = 250
        private const val DEFAULT_POINT_SIZE = 7F
        private val DEFAULT_COLOR = Triple(255F / 255F, 74F / 255F, 30F / 255F)
    }

    private var vbo = 0
    private var vboSize = 0
    private var pointsNumber = 0
    private var shaderProgram = 0
    private var positionAttribute = 0
    private var colorUniform = 0
    private var modelViewProjectionUniform = 0
    private var pointSizeUniform = 0

    private var lastPointCloud: PointCloud? = null

    fun initOnGLThread() {
        initVbo()
        initShadersAndProgram(context)
        bindShaderParams()
    }

    fun update(pointCloud: PointCloud) {
        if (pointCloud == lastPointCloud) return
        checkPointCloudSizeAndUploadDataToVbo(pointCloud)
    }

    fun render(cameraView: FloatArray, cameraPerspective: FloatArray) {
        val modelViewProjection = FloatArray(16)
        Matrix.multiplyMM(modelViewProjection, 0, cameraPerspective, 0, cameraView, 0)
        drawPoints(modelViewProjection)
    }

    fun pointsNumber() = pointsNumber

    private fun initVbo() {
        checkGLError(TAG, "Before init")
        val buffers = IntArray(1)
        GLES20.glGenBuffers(1, buffers, 0)
        vboSize = INITIAL_BUFFER_POINTS * BYTES_PER_POINT
        vbo = buffers[0]
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo)
        GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vboSize, null, GLES20.GL_DYNAMIC_DRAW)
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0) // unbind vbo
        checkGLError(TAG, "Buffer allocated")
    }

    private fun loadShaders(context: Context): Pair<Int, Int> {
        val vertexShader = ShaderLoader.loadGLShader(TAG, context, GLES20.GL_VERTEX_SHADER, VERTEX_SHADER_NAME)
        val fragmentShader = ShaderLoader.loadGLShader(TAG, context, GLES20.GL_FRAGMENT_SHADER, FRAGMENT_SHADER_NAME)
        return Pair(vertexShader, fragmentShader)
    }

    private fun initShadersAndProgram(context: Context) {
        val (vertex, fragment) = loadShaders(context)
        shaderProgram = GLES20.glCreateProgram()
        GLES20.glAttachShader(shaderProgram, vertex)
        GLES20.glAttachShader(shaderProgram, fragment)
        GLES20.glLinkProgram(shaderProgram)
        GLES20.glUseProgram(shaderProgram)
        checkGLError(TAG, "Shader program creating")
    }

    private fun bindShaderParams() {
        positionAttribute = GLES20.glGetAttribLocation(shaderProgram, "a_Position")
        colorUniform = GLES20.glGetUniformLocation(shaderProgram, "u_Color")
        modelViewProjectionUniform = GLES20.glGetUniformLocation(shaderProgram, "u_ModelViewProjection")
        pointSizeUniform = GLES20.glGetUniformLocation(shaderProgram, "u_PointSize")
        checkGLError(TAG, "Linking program parameters")
    }

    private fun checkPointCloudSizeAndUploadDataToVbo(pointCloud: PointCloud) {
        checkGLError(TAG, "Before update")
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo)
        lastPointCloud = pointCloud
        lastPointCloud?.let {
            pointsNumber = it.points.remaining() / FLOATS_PER_POINT
            if (pointsNumber * BYTES_PER_POINT > vboSize) {
                while (pointsNumber * BYTES_PER_POINT > vboSize) {
                    vboSize *= 2
                }
                GLES20.glBufferData(GLES20.GL_ARRAY_BUFFER, vboSize, null, GLES20.GL_DYNAMIC_DRAW)
            }
            GLES20.glBufferSubData(GLES20.GL_ARRAY_BUFFER, 0, pointsNumber * BYTES_PER_POINT, it.points)
        }
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0) // unbind vbo
        checkGLError(TAG, "after vbo update")
    }

    private fun drawPoints(modelViewProjection: FloatArray) {
        val (x, y, z) = DEFAULT_COLOR
        checkGLError(TAG, "Before draw")
        GLES20.glUseProgram(shaderProgram)
        GLES20.glEnableVertexAttribArray(positionAttribute)
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, vbo) // bind vbo
        GLES20.glVertexAttribPointer(positionAttribute, 4, GLES20.GL_FLOAT, false, BYTES_PER_POINT, 0)
        GLES20.glUniform4f(colorUniform, x, y, z, 1F)
        GLES20.glUniformMatrix4fv(modelViewProjectionUniform, 1, false, modelViewProjection, 0)
        GLES20.glUniform1f(pointSizeUniform, DEFAULT_POINT_SIZE)
        GLES20.glDrawArrays(GLES20.GL_POINTS, 0, pointsNumber)
        GLES20.glDisableVertexAttribArray(positionAttribute)
        GLES20.glBindBuffer(GLES20.GL_ARRAY_BUFFER, 0) // unbind vbo
        checkGLError(TAG, "Rendering")
    }

}
